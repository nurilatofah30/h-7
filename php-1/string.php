<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>String PHP</title>
</head>
<body>
<h1>Berlatih String PHP</h1>
<h3> Soal No 1</h3>
<?php 
$kalimat = "Hello PHP!";
?>
<?php
$panjang_string=strlen($kalimat); 
echo "panjang string = $panjang_string string";
echo "<br>";

?>
<?php
$jumlah_kata=str_word_count($kalimat);
echo "jumlah kata = $jumlah_kata kata";

?>
<br>
<?php
$second_sentence =  "I'm ready for the challenges";
?>
<?php
$panjang_string=strlen($second_sentence); 
echo "panjang string = $panjang_string string";
echo "<br>";

?>
<?php
$jumlah_kata=str_word_count($second_sentence);
echo "jumlah kata = $jumlah_kata kata";

?>
<h3> Soal No 2</h3>

<?php
$string2 = "I love PHP";
 echo "<label>String: </label> \"$string2\" <br>"; 
 echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
 echo "Kata kedua:". substr($string2,2,4). "<br>";// Lanjutkan di bawah ini echo "Kata kedua: " ; echo "<br> Kata Ketiga: " ; echo "<h3> Soal No 3 </h3>"; 
 echo "Kata ketiga:". substr($string2, 6,7 ). "<br>";// Lanjutkan di bawah ini echo "Kata kedua: " ; echo "<br> Kata Ketiga: " ; echo "<h3> Soal No 3 </h3>"; 
 ?>
 <h3> Soal No 3</h3>
 <?php
/* SOAL NO 3 Mengubah karakter atau kata yang ada di dalam sebuah string. */
$string3 = "PHP is old but sexy!"; 
echo "String: \"$string3\ ";
echo "str_replace"("sexy","awesome", $string3);
 // OUTPUT : "PHP is old but awesome" ?>